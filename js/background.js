// Запуск приветственного окна
chrome.runtime.onInstalled.addListener(async () => {
	let url = chrome.runtime.getURL('html/hello.html');
	let tab = await chrome.tabs.create({ url });

	chrome.storage.sync.get(['showClock'], (result) => {
		if (result.showClock)
		{
			chrome.action.setBadgeText({ text: 'ON' });
		}
	});

	chrome.storage.sync.get(['timer'], (result) => {
		console.log('result', result);
		if (!result.timer)
		{
			chrome.storage.sync.set({ 'timer': 1 });
		}
	});
});

// Работа с командами из манифеста
chrome.commands.onCommand.addListener((command) => {

	chrome.tabs.update({}, async (tab) => {
		if (command === 'my-command')
		{
			chrome.tabs.update({ pinned: !tab.pinned });
		}
	});
});
